const student = {
    name: '',
    lastName: ''
}

student.name = prompt("Введіть імя студента:");
student.lastName = prompt("Введіть прізвище студента:");

let subject = '';
let grade = 0;
let table = {};

while (subject !== null) {
    subject = prompt('Введіть назву предмета:');
    if (subject !== null) {
        grade = parseFloat(prompt('Введіть оцінку за предмет' + subject + ':'));
        table[subject] = grade;
    }
}
console.log(table);

let badGrades = 0;

for (let subject in table) {
    if (table[subject] < 4) {
        badGrades++;
    }
}

if (badGrades > 0) {
    console.log(`Студент має ${badGrades} поганих оцінок.`);
} else {
    console.log(`Студента ${student.lastName} ${student.name} переведено на наступний курс.`)
}

let sumGrades = 0;
let numGrades = 0;

for (let subject in table) {
  sumGrades += table[subject];
  numGrades++;
}

let avgGrade = sumGrades / numGrades;

console.log('Середній бал: ' + avgGrade.toFixed(2));

if (avgGrade > 7) {
  console.log('Студенту призначено стипендію.');
}